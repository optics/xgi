% ******************************************************************************
%
%   Copyright (C) 2021 Paul Scherrer Institut Villigen, Switzerland
%   
%   Author Juraj Krempasky, juraj.krempasky@psi.ch
%          Uwe Flechsig,    uwe.flechsig@psi.ch
%
% ------------------------------------------------------------------------------
%
%   This file is part of xgi.
%
%   xgi is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, version 3 of the License, or
%   (at your option) any later version.
%
%   xgi is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with xgi (LICENSE).  If not, see <http://www.gnu.org/licenses/>. 
%
% ******************************************************************************

function [out] = xgi_func2(img, ip) 
ROC=0;
laxis=[];
wf=[]; hp=[];
aux=[];
out=struct;

[cols, rows] = size(img);

beta = -(ip.beta_rot - ip.beta_fix)/2;

    dim_ana = 2;dim_ana_t=1;
    % separate Hann window for each row
    %hanning_window = repmat(hanning(size(img,2)),1,size(img,1))';
    %s = myhanning(cols);
    nn=1:1:cols;
    HANN = .54 - 0.46*cos(2*pi.*nn/(cols+1));
    HANN=HANN';
    hanning_window = repmat(HANN,1,rows);%here removes the ' operator for Matlab2018

% zero padded, by a factor of 16
% Fourier transform along dimension 2 (rows) with hanning window and zero-padding
fimg = fft((img).*hanning_window,ip.n_zeropadding*size(img,2),dim_ana);
% projection of the FFT image on the orthogonal axis system


% mean of Fourier transform along the direction of interest (zero-padded by
% factor of 16)
mean_proj = mean(abs(fimg),dim_ana_t);
out.fft = mean_proj;

% get the position of the maximum value -> DC component position
[val_M, pos_ind_M] = max(mean_proj);
% get the position where the intensity is half the maximum value in order to have an estimate for the FWHM (Gaussian approximation of the DC component)
[~,pos_ind_fwhm] = min(abs(mean_proj-val_M/2));
% calculate from the FWHM the width of the Gaussian
G_width = 2*abs(pos_ind_M - pos_ind_fwhm) / 2.35;
%ensure max G_width
fprintf(2, 'pos_ind_M=%f pos_ind_fwhm=%f  G_width=%f ', pos_ind_M, pos_ind_fwhm, G_width);
if G_width>10
    G_width=10;    
end

% find the correct Fourier peak
[val_m,pos_ind_p] = max(mean_proj(pos_ind_M+round(ip.excl_para*G_width):length(mean_proj)/2));
pos_ind_p = pos_ind_p+pos_ind_M+round(ip.excl_para*G_width)-1;
%shift
freq = pos_ind_p-pos_ind_M;

N2 = size(fimg,2);
period = N2*ip.pixel_size/freq * 10^6;
fprintf(2, 'period=%f \n',period)

% finge visibility
visib = val_m/val_M;
fprintf(2,  'freq=%f  fringe visib=%.3f \n',freq, visib);

% mask which will be the filter
line_mask = zeros(size(fimg,1), size(fimg,2));
filter_width = round(ip.filter_para*G_width);
% 0-order and first order component processing

    % filtering out the frequency of interest
    line_mask(:, 1:filter_width+1) = 1;
    line_mask(:, end-filter_width:end) = 1;
    % mask out zero order
    fimg_amp = fimg;
    fimg_amp(~line_mask) = 0;
    % shifts fourier transform (complex) so that frequency of interest is
    % at zero
    fimg = circshift(fimg, [0 -round(freq)]);
    % sets everything outside mask to zero
    fimg(~line_mask) = 0;

    %%%%%%%%%%%%% changed here %%%%%%%%%%%%
    fimg = circshift(fimg, [0 round(freq)]);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
% 0-rder -> intensity on camera
amp = abs(ifft(fimg_amp,[],dim_ana));

% crop back to original image size
amp = amp(1:size(img,1), 1:size(img,2));
amp = amp ./ (hanning_window);
out.amp = amp;

%amp = sum(sum(amp(ncrop:end-ncrop+1,ncrop:end-ncrop+1),2));
%fprintf(2, 'intensity on image: %.0f \n',amp);

%  inverse Fourier transform
ifimg = ifft(fimg,[],dim_ana);

% cropping in the inverse Fourier transformed image (back to original size)
ifimg = ifimg(1:size(img,1), 1:size(img,2));


out.ifimg = ifimg;

% differential fringe phase; returns the phase angle in radians
dphi_wrapped = angle(ifimg); %this is atan -> values netween -pi,pi -> need to unwrap
out.dphi_wrap = dphi_wrapped;

dphi_unwrap = unwrap(dphi_wrapped,pi,2);
dphi_unwrap = unwrap(dphi_unwrap,pi,1);
%dphi_unwrap = double(Miguel_2D_unwrapper(single(dphi_wrapped)));
out.dphi_unwrap = dphi_unwrap;

% coordinates in units of pixels
[ Y, X ] = ndgrid(size(img,1):-1:1, 1:size(img,2));

% fit a first order polynom through the unwrapped phase 
% which is due to the fringe fundamental frequency, 
%doing this is equvialent to mTakeda1984's shifting in fourier space

    polys = zeros(2,size(img,1));
    for ii = 1:size(img,1)
        poly_tmp = polyfit(X(ii,:),dphi_unwrap(ii,:),1);
        polys(1,ii) = poly_tmp(1);
        polys(2,ii) = poly_tmp(2);
    end
    dphi = dphi_unwrap;
    
% alpha wavefront propagation angle; Eq. 2.74 Rutishauser thesis(calibration params)
alpha = (ip.M0*cos(ip.beta_fix)/cos(ip.beta_rot) - 1)*Y*ip.pixel_size/ip.d + ip.M0*ip.p2*cos(ip.beta_fix)/(2*pi*ip.d*(cos(ip.beta_rot))^2) * dphi;
% first order polynomial fit of the propagation angle of the wavefront through the image
polys = zeros(1,size(alpha,1)-2*ip.ncrop+2);


%%%%%%%% changed here %%%%%%%%%%%
for ii = ip.ncrop:size(alpha,1)-ip.ncrop+1
       poly_tmp = polyfit(X(ii,:),alpha(ii,:),1);
       polys(1,ii-ip.ncrop+1) = poly_tmp(1);
end   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


out.polys = polys;
out.alpha2D = alpha;
%% 
% run-time radius of curvature ROC determination
% connected through a geometric consideration to the propagation angle of the wavefront; Rutishauser thesis Eq. 2.26 
ROC = ip.pixel_size/abs(mean(polys));
dROC = ip.pixel_size/(abs(mean(polys))^2)*std(polys);
%
fprintf(2, 'ROC %.3f +/- %.3f m \n', ROC, dROC);
out.ROC=ROC;
out.dROC=dROC;
%% find center of mass the image
X_hist=sum(img,2)'; X=1:size(img,1); 
fprintf(2, 'X_hist %f X %f', size(X_hist),size(X))
i_x=round(sum(X.*X_hist)/sum(X_hist));

laxis = (1:size(img,1))*ip.pixel_size;
out.laxis=laxis;
%slope_data = alpha(:, i_x)';  

%%%%%%%%%%%%%%% changed here %%%%%%%%%%%%%%%
slope_data = alpha(i_x,:);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

slope_data = slope_data-mean(slope_data);
out.wpa=slope_data;

phi_y1 = cumsum( slope_data'*ip.pixel_size );

%yaxis = (1:size(img,1))*ip.pixel_size;

%%%%%%%%%%%%%%% changed here %%%%%%%%%%%%%%
yaxis = (1:size(img,2))*ip.pixel_size;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% second order polynomial fit (spherical component)
py_phi = polyfit(yaxis', phi_y1, 2);

phi_y_aspherical = phi_y1 - polyval(py_phi, yaxis');


%% height profile




dphi_quant = ip.p2 / (2*pi * ip.d);
% scale for the image [m]
xaxis = (1:size(img,2))*ip.pixel_size;
% scale for the image [m]
yaxis = (1:size(img,1))*ip.pixel_size;

slope_data_y=slope_data';
 % creating a rectangular grid with same dimensions as the data for the propagation angles
[ Yoffset_y, Xoffset_y ] = ndgrid(1:size(slope_data_y,1), 1:size(slope_data_y,2));
nbin=1;
    [ Yoffset_y_all, Xoffset_y_all ] = ndgrid(1:size(slope_data_y,1), 1:size(slope_data_y,2));
    phi_y_quant = ip.p2 / (ip.lambda * ip.d) * ip.pixel_size;
    phi_y_surface_profile_quant = phi_y_quant * ip.lambda/(2*pi); %lambda is in meters
    phi_y = cumsum( slope_data_y / dphi_quant, 1);
    out.phi_y=phi_y * phi_y_surface_profile_quant;
    %out.phi_y=cumsum( slope_data_y, 1);
    % second order polynomial fit (spherical component)
    py_phi = polyfit(xaxis', phi_y, 2);
   
    % aspherical
    %phi_y_aspherical = phi_y - repmat_size(polyval(py_phi, yaxis'), size(phi_y)); 
    phi_y_aspherical = phi_y - polyval(py_phi, xaxis');
    % metric dimension on the image
    eff_pix_size = ip.pixel_size;

out.yaxis=yaxis;
% height profile hp
hp=phi_y_aspherical*phi_y_surface_profile_quant;
hp = hp - mean(hp);
out.hp = hp
