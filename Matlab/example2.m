% ******************************************************************************
%
%   Copyright (C) 2021 Paul Scherrer Institut Villigen, Switzerland
%   
%   Author Juraj Krempasky, juraj.krempasky@psi.ch
%          Uwe Flechsig,    uwe.flechsig@psi.ch
%
% ------------------------------------------------------------------------------
%
%   This file is part of xgi.
%
%   xgi is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, version 3 of the License, or
%   (at your option) any later version.
%
%   xgi is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with xgi (LICENSE).  If not, see <http://www.gnu.org/licenses/>. 
%
% ******************************************************************************

clear all;
dir_path = struct;
%uf dir_path.expdata = '.\';      % windows
dir_path.expdata = '';   %'./';   % linux

%parameter initializations (input comes from moire XGI calibration)
beta_fix=0.0271; %fixed absolute angle on phase grating
beta_rot=-0.0185; %rotated absolte angle on absorption grating 

%reading moire interferograms and dark images (we have two) 
img = read_img(dir_path,87,'sfb_');
dark = read_img(dir_path,61:62,'sfb_');
img=img-mean(dark,3);

roi = struct;
roi.x1= 506;roi.x2= 827;roi.y1= 191;roi.y2= 525; %this roi is found to well center the moire interf.
img=img(roi.y1:roi.y2,roi.x1:roi.x2);

ip = struct;
ip.beta_fix=beta_fix;
ip.beta_rot=beta_rot;

% period of G1 [m]  phase grating
p1=3.75e-6;
ip.p1 = p1;
% period of G2 [m]  absorption grating
p2 = 2.000e-6;
ip.p2 = p2;
% intergrating (G1-G2) distance [m]
ip.d = 144e-3;
%ip.d = 152e-3;
% % fractional Talbot order
ip.order = 11;
% X-ray energy [keV]
ip.kev = 9;

% X-ray wavelength [m]
ip.lambda = 12.3984191/ip.kev*1E-10;    %*power(10,-10);
% magnification
ip.M0 =p2/p1*2;
% camera pixel size [m]
ip.pixel_size = 2.857e-6;
% some convenience parameters for the FFT filter
% zeropadding parameter
ip.n_zeropadding = 16; % n_zeropadding = 4; % must be power of 2
% parameter for search region in fourier space
ip.excl_para = 13;
% parameter for filter region in fourier space
ip.filter_para = 3;
% cropping after FFT
ip.ncrop = 50;


M=img;

[out] = xgi_func2(M,ip)

%show data output
ft=fftshift(out.fft);
amp =  abs(out.amp);
ifimg = abs(out.ifimg);
wrap=out.dphi_wrap;
unwrap=out.dphi_unwrap;
polys = out.polys;

figure
subplot(1,6,1); 
imagesc(M);colormap(flipud(bone)); title('moire image')
colormap default
ax_ft=subplot(1,6,2); plot(log(ft));set(ax_ft, 'YTick','');title('1D FFT')
subplot(1,6,3); imagesc(ifimg);title('1st order FFT filter')
subplot(1,6,4); imagesc(wrap);title('wrapped fringe phase')
ax2= subplot(1,6,5); plot(out.hp*1E9);title('height profile');ylabel('nanometers')
ax2 = subplot(1,6,6); plot(out.phi_y*1E9);title('spherical wavefront');ylabel('nanometers')


%%


