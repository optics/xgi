# Makefile for standard linux distributions
# tested on RH7 and SUSE TW
# Version Oct 2021 

# ******************************************************************************
#
#   Copyright (C) 2021 Paul Scherrer Institut Villigen, Switzerland
#   
#   Author Juraj Krempasky, juraj.krempasky@psi.ch
#          Uwe Flechsig,    uwe.flechsig@psi.ch
#
# ------------------------------------------------------------------------------
#
#   This file is part of xgi.
#
#   xgi is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, version 3 of the License, or
#   (at your option) any later version.
#
#   xgi is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with xgi (LICENSE).  If not, see <http://www.gnu.org/licenses/>. 
#
# ******************************************************************************


# tools
CC = g++

# switches
DEBUG	= -DDEBUG
DEFS    = -DLINUX -DUSE_DOUBLE_PRECISION -DHAVE_TIFF -DHAVE_PNG -DHAVE_HDF5 $(DEBUG)
CFLAGS  = $(DEFS) -O3 -std=c++0x -c
LDFLAGS = -lm 
LIBS    = -ltiff -lpng -lhdf5 -lfftw3

# 
.phony: all
PROGRAM = PsiXgiDemo
SRCS = main.cpp xgi.cpp xgi.h
OBJS = main.o xgi.o

# rules
all:  $(PROGRAM)

##########################################################################
$(PROGRAM): $(OBJS) 
	$(CC) -o $@ $(OBJS) $(LDFLAGS) $(LIBS)


%.o: %.cpp 
	$(CC) $(CFLAGS) -o $@ $< 

TAGS:   $(SRCS)
	etags $(SRCS)

clean:
	rm -f *.o core $(PROGRAM)
# end
