#include <cstdio>
#include "xgi.h"
#define DPATH "."
//#define DPATH "orig"

using namespace std;

int main(int argc, char **args)
{
    int dbgstep = 0;
    Xgi x = Xgi();
    Xgi_ip ip;
    Matrix put_in;
    Xgi_out put_out;
    /* load data
    img = read_img_ProSilica(dir_path,87,'sfb_');
    dark = read_img_ProSilica(dir_path,61:62,'sfb_');
    img=img-mean(dark,3);
    */
    Matrix dark,misc;
    char fn[512];
    sprintf(fn, "%s/sfb_%04d.h5", DPATH, 87);

#ifdef DEBUG
    printf("debug: step %d\n", dbgstep++);
#endif

    put_in.load_h5_ushort(fn, "/entry/instrument/detector/data", 0);
    dark.zero(put_in.width, put_in.height);

#ifdef DEBUG
    printf("debug: step %d\n", dbgstep++);
#endif    

    int dark_low = 61;
    int dark_high = 62;
    for (int i=dark_low; i<=dark_high; ++i)
    {   
        sprintf(fn, "%s/sfb_%04d.h5", DPATH, i);
        misc.load_h5_ushort(fn,"/entry/instrument/detector/data",0);
        dark.add(&misc); 
    }

#ifdef DEBUG
    printf("debug: step %d\n", dbgstep++);
#endif 

    if (dark_low <= dark_high)
        dark.mul(C_ONE/(dark_high +1 - dark_low));

#ifdef DEBUG
    printf("debug 3: step %d\n", dbgstep++);
#endif

    // put_in.save_tiff("input.tif");
    put_in.save_png("input.png");
    put_in.sub(&dark);

#ifdef DEBUG
    printf("debug 4: step %d\n", dbgstep++);
#endif    

    //ip.verbose = 9001;
    ip.n_zeropadding = 16;
    //ip.ncrop = 128;
    //put_in.data.save_tiff("testi.tif");
    //put_in.data.save_png("testi.png",INFINITY,300);
    printf("Compute ... \n");
    x.compute(&put_in,&ip,&put_out);
    printf("done\n");
    //put_out.fft_re.save_tiff("test.tif");
    put_out.roi.save_png("roi.png");
    put_out.fft_re.save_png("fftre.png");
    put_out.fft_im.save_png("fftim.png");
    put_out.fft_re.save_txt("fftre.txt");
    put_out.fft_im.save_txt("fftim.txt");
    put_out.fft.save_png("fft.png");
    put_out.amp.save_png("amp.png");
    put_out.ifimg_re.save_png("ifftre.png");
    put_out.ifimg_im.save_png("ifftim.png");
    put_out.dphi_wrap.save_png("dphi_w.png");
    put_out.dphi_unwrap.save_png("dphi_uw.png");
    put_out.dphi.save_png("dphi.png");
    //put_out.polys.print();
    put_out.polys.save_png("polys.png");
    put_out.alpha.save_png("alpha.png");
    put_out.alpha2D.save_png("alpha2D.png");
    put_out.wpa.save_png("wpa.png");
    put_out.phi_y.save_png("phi_y.png");
    put_out.yaxis.save_png("xaxis.png");
    put_out.hp.save_png("hp.png");
}
// end main.cpp

