INSTRUCTIONS
============

C-implementation
================
This folder contains C++ code for wavefront analysis (XGI) of an image saved in sfb_0087.h5 file. The code is subtacting an average of two dark images ( sfb_0061.h5 and  sfb_0062.h5 ) before proceeding with the analysis. The code processing the XGI is generating images in order to follow and verify the processing pipeline, as described in Ref.[1]. The final output is a text file containig the wavefront profile in nanometers. 

It should be noted that the C++ code was not generated by Matlab but is written from scratch. The Makefile contains FFTW_INCLUDE = /home/optics/Software/fftw/include variable which points to a separate fftw3 installation downloaded from http://www.fftw.org/download.html. The reason was the default FFTPACK library available on default Scientific Linux installation was very slow. On the other hand the hdf5 library accessed with -I/usr/include/hdf5/serial is used referring to the pre-installed HDF software. The paths in the Makefile must be adjusted to build the software on your computer (see comments in the Makefile).  

Matlab
======
This folder contains Matlab scripts equivalent to the C++ implementation. 

References:
==========
[1] Krempask{\'y}, J. and Koch, F. and Vagovi{\vc}, P. and Mike{\vs}, L. and   Jaggi, A. and Svetina, C. and Flechsig, U. and Patthey, L. and Marathe, S. and  Batey, D. and Cipiccia, S. and Rau, C. and Seiboth, F. and Seaberg, M. and David, C. and Wagner, U.H., Inspecting adaptive optics with at-wavelength wavefront metrology, Proc.SPIE, 10761, 10761 - 10761 - 8, 2018.

[2] M. Seaberg et al.,Wavefront sensing at X-ray free-electron lasers, J. Synchrotron Rad. 26, 1115-1126, (2019)
https://doi.org/10.1107/S1600577519005721

Acknowledgements:
=================
Many people contributed to this work. The Matlab XGI implementation started by Y.Kayser (BESSY) was reimplemented by J.Krempasky and futher reviewed/optimized during the collaborative work (see Ref.2) by M. Seaberg (SLAC), P. Vagovic and L. Mikes (DESY).    
